import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import $ from 'jquery';
import * as moment from "moment";

enum METHOD {
	GET,
	POST,
	PUT,
	DELETE
}

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

	constructor(private http: HttpClient) {

	}

	public set waiting(value) {
		if (value === true)
			$(".spinner-container").addClass("d-block");
		else
			$(".spinner-container").removeClass("d-block");
	}

	source = [];

	supervisores = [];
	clases = [];
	departamentos = [];
	subsidiarias = [];

	file: File = null;

	form = new FormGroup({
		file: new FormControl(null, [Validators.required]),
		sheet: new FormControl("Employees", [Validators.required])
	});

	formSearch = new FormGroup({
		supervisor: new FormControl("-1", []),
		clase: new FormControl("-1", []),
		departamento: new FormControl("-1", []),
		subsidiaria: new FormControl("-1", []),
	});

	invoke(tipo: METHOD, endpoint, data = null, settings = {}) {
		let self = this;

		let promesa = new Promise((resolve, reject) => {
			this.waiting = true;

			let url = `https://localhost:5001/api/employees/${endpoint}`;
			let service = null;

			switch (tipo) {
				case METHOD.POST:
					service = self.http.post(url, data, settings);
					break;
				case METHOD.PUT:
					service = self.http.put(url, data);
					break;
				case METHOD.DELETE:
					service = self.http.delete(url);
					break;
				default:
					service = self.http.get(url);
					break;
			}

			setTimeout(() => {
				service
					.subscribe(
						info => {
							console.log('SUCCESS: ', info);

							self.waiting = false;

							resolve(info);
						},
						ex => {
							console.log('ERROR: ', ex);

							self.waiting = false;

							reject(ex);
						}
					);
			}, 500);
		});

		return promesa;
	}

	ngOnInit() {
		console.log('ngOnInit');

		this.cargarFiltros();
	}

	cargarFiltros() {
		const self = this;

		this.invoke(METHOD.GET, 'cargar_filtros').then(
			info => {
				var data = Object.assign({
					supervisores: [],
					clases: [],
					departamentos: [],
					subsidiarias: [],
				}, info);

				this.supervisores = data.supervisores;
				this.clases = data.clases;
				this.departamentos = data.departamentos;
				this.subsidiarias = data.subsidiarias;

				this.source = [];
			}
		);
	}

	setFile(archivo) {
		this.form.get("file").setValue(archivo.target.value);
		this.file = archivo.target.files[0]
	}

	cargarArchivo() {
		this.alta_masiva(this.file, this.form.get('sheet').value);
	}

	alta_masiva(archivo: File, hoja: String) {
		const data = new FormData();
		const self = this;

		data.append("archivo", archivo);

		this.invoke(METHOD.POST, `cargar_excel/${hoja}`, data).then(
			info => {
				this.source = [];
				this.form.reset();
				this.formSearch.reset();
				this.cargarFiltros();
			});
	}

	buscarDatos() {
		const filtro = this.formSearch.value;

		this.invoke(METHOD.POST, "cargar_empleados", filtro).then(
			(info) => {
				this.source = <[]>info;
			}, error => { });
	}

	descargarEmpleados(formato) {
		let input = {
			"filtro": this.formSearch.value,
			"source": this.source
		};

		this.invoke(METHOD.POST, `descargar_empleados/${formato}`, input, { responseType: 'blob' })
			.then(response => {
				console.log('r: ', response);

				let blob = new Blob([<BlobPart>response], { type: 'application/octet-stream' });
				let link = document.createElement('a');
				let fileName = `EmployeesReport_${moment(new Date()).format('YYYYMMDD-HHmmss')}.${formato.toLowerCase()}`;
				//let fileName = `EmployeesReport_${new Date().toLocaleTimeString()}.${formato.toLowerCase()}`;

				link.href = window.URL.createObjectURL(blob);
				link.download = fileName;
				link.click();
			});
	}
}
