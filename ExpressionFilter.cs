﻿using System;
namespace KisforcePruebaTecnica
{
    public class ExpressionFilter
    {
        public string PropertyName { get; set; }
        public string Value { get; set; }

        public ExpressionFilter()
        {

        }

        public ExpressionFilter(string name, string value)
        {
            this.PropertyName = name;
            this.Value = value;
        }
    }
}
