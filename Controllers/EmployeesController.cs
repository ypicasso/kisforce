using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using KisforcePruebaTecnica.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;

using iTextSharp.text;
using iTextSharp.text.pdf;

namespace KisforcePruebaTecnica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly string FILE_NAME = "employees.json";
        private readonly string FILE_PATH = "";

        public EmployeesController(IWebHostEnvironment env)
        {
            FILE_PATH = env.ContentRootPath;
        }

        private async Task<List<Employee>> GetEmployees()
        {
            var items = new List<Employee>();
            var path = Path.Combine(FILE_PATH, FILE_NAME);

            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var sr = new StreamReader(fs, System.Text.Encoding.UTF8))
                {
                    string json = await sr.ReadToEndAsync();

                    items = JsonSerializer.Deserialize<List<Employee>>(json);
                }
            }

            return items;
        }

        private async Task<IActionResult> ObtenerEmpleados()
        {
            try
            {
                var items = await GetEmployees();

                return Ok(new
                {
                    clases = items.GroupBy(g => g.Clase).Select(s => s.Key).OrderBy(o => o).ToList(),
                    supervisores = items.GroupBy(g => g.Supervisor).Select(s => s.Key).OrderBy(o => o).ToList(),
                    subsidiarias = items.GroupBy(g => g.Subsidiaria).Select(s => s.Key).OrderBy(o => o).ToList(),
                    departamentos = items.GroupBy(g => g.Departamento).Select(s => s.Key).OrderBy(o => o).ToList(),
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("cargar_filtros")]
        public async Task<IActionResult> cargar_filtros() => await ObtenerEmpleados();

        [HttpPost("cargar_excel/{sheet}"), DisableRequestSizeLimit]
        public async Task<IActionResult> cargar_excel(string sheet)
        {
            List<Employee> employees = new List<Employee>();

            try
            {
                var files = Request.Form.Files;

                if (files.Count > 0)
                {
                    var archivo = files.First();

                    if (!Regex.IsMatch(Path.GetExtension(archivo.FileName), @"\.xlsx?$", RegexOptions.IgnoreCase))
                        return BadRequest("Solo son permitidos archivos de tipo Excel 2007 en adelante (.xlsx)");

                    byte[] data = null;

                    using (var ms = new MemoryStream())
                    {
                        await archivo.CopyToAsync(ms);

                        data = ms.ToArray();
                    }

                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                    using (MemoryStream stream = new MemoryStream(data))
                    using (ExcelPackage excelPackage = new ExcelPackage(stream))
                    {
                        var excelSheet = excelPackage.Workbook.Worksheets.FirstOrDefault(w => w.Name.Equals(sheet.Trim(), StringComparison.OrdinalIgnoreCase));

                        if (excelSheet == null)
                            return BadRequest("La hoja indicada no existe en el archivo de carga");

                        var colCount = excelSheet.Dimension.End.Column;
                        var rowCount = excelSheet.Dimension.End.Row;

                        if (colCount < 6)
                            return BadRequest("La hoja indicada no cuenta con todas las columnas correspondientes");

                        var columns = new Dictionary<int, string>()
                        {
                            {1, "Nombre"},
                            {2, "Cargo"},
                            {3, "Supervisor"},
                            {4, "Clase"},
                            {5, "Subsidiaria"},
                            {6, "Departamento"},
                        };

                        var isMatchColumns = true;

                        for (int i = 1; i <= columns.Count; i++)
                        {
                            var colName = excelSheet.Cells[1, i].GetValue<string>().Trim().ToUpper();

                            if (!colName.Equals(columns[i], StringComparison.OrdinalIgnoreCase))
                            {
                                isMatchColumns = false;
                                break;
                            }
                        }

                        if (!isMatchColumns)
                        {
                            var columnas = string.Join(", ",
                                columns.OrderBy(o => o.Key).Select(s => s.Value).ToArray()
                            );

                            return BadRequest($"Las columnas de la hoja excel deben de ser las siguientes: {columnas}");
                        }

                        for (int i = 2; i <= excelSheet.Dimension.End.Row; i++)
                        {
                            #region Validación de todos los datos vacios...

                            var counter = 0;

                            for (int j = 1; j <= colCount; j++)
                            {
                                var value = excelSheet.GetString(i, j);

                                if (value == null || string.IsNullOrEmpty(value))
                                    counter++;
                            }

                            if (counter == colCount)
                                continue;

                            #endregion

                            Debug.WriteLine("Linea : {0}", i);

                            var item = new Employee();

                            item.Fila = i - 1;
                            item.Nombre = excelSheet.Cells[i, 1].GetValue<string>()?.Trim();
                            item.Cargo = excelSheet.Cells[i, 2].GetValue<string>()?.Trim();
                            item.Supervisor = excelSheet.Cells[i, 3].GetValue<string>()?.Trim();
                            item.Clase = excelSheet.Cells[i, 4].GetValue<string>()?.Trim();
                            item.Subsidiaria = excelSheet.Cells[i, 5].GetValue<string>()?.Trim();
                            item.Departamento = excelSheet.Cells[i, 6].GetValue<string>()?.Trim();


                            employees.Add(item);
                        }
                    }

                    var json_data = JsonSerializer.Serialize(employees);

                    try
                    {
                        var path = Path.Combine(FILE_PATH, FILE_NAME);

                        using (var fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
                        {
                            using (var sw = new StreamWriter(path, false, System.Text.Encoding.UTF8))
                            {
                                await sw.WriteAsync(json_data);
                            }
                        }
                    }
                    catch (Exception fex)
                    {

                    }

                    return Ok(employees);
                }
                else
                {
                    return BadRequest("No se envió archivo a procesar");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("cargar_empleados")]
        public async Task<IActionResult> cargar_empleados([FromBody] Employee filtro)
        {
            try
            {
                var filtros = new List<ExpressionFilter>();
                var items = await GetEmployees();

                if (filtro != null)
                {
                    const string m1 = "-1";

                    if (filtro.Clase != null && !filtro.Clase.Equals(m1)) filtros.Add(new ExpressionFilter("Clase", filtro.Clase));
                    if (filtro.Supervisor != null && !filtro.Supervisor.Equals(m1)) filtros.Add(new ExpressionFilter("Supervisor", filtro.Supervisor));
                    if (filtro.Subsidiaria != null && !filtro.Subsidiaria.Equals(m1)) filtros.Add(new ExpressionFilter("Subsidiaria", filtro.Subsidiaria));
                    if (filtro.Departamento != null && !filtro.Departamento.Equals(m1)) filtros.Add(new ExpressionFilter("Departamento", filtro.Departamento));
                }

                if (filtros.Count > 0)
                {
                    var source = new List<Employee>(items.ToArray());

                    items = source.Where(ExpressionBuilder.GetExpression<Employee>(filtros).Compile()).ToList();
                }

                return Ok((
                    from i in items
                    orderby i.Supervisor
                    group i by i.Supervisor into g
                    select new Employee
                    {
                        Supervisor = g.Key,
                        Items = g.OrderBy(o => o.Nombre).ToList()
                    }
                ));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("descargar_empleados/{formato}")]
        public async Task<IActionResult> descargar_empleados(string formato, [FromBody] ReportInput input)
        {
            MemoryStream memo = null;

            var imagePath = Path.Combine(FILE_PATH, "kis-logo-vertical.png");
            var whiteColor = ColorTranslator.FromHtml("#FFFFFF");
            var brownColor = ColorTranslator.FromHtml("#444444");
            var orangeColor = ColorTranslator.FromHtml("#C8801D");

            await Task.Run(() =>
            {
                if (formato.Equals("xlsx", StringComparison.OrdinalIgnoreCase) || formato.Equals("csv", StringComparison.OrdinalIgnoreCase))
                {
                    #region Excel o CSV...

                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                    using (ExcelPackage excel = new ExcelPackage())
                    {
                        var sheet = excel.Workbook.Worksheets.Add("EmployeesReport");

                        // Colocar imagen
                        using (System.Drawing.Image image = System.Drawing.Image.FromFile(imagePath))
                        {
                            var excelImage = sheet.Drawings.AddPicture("Logo", image);

                            //add the image to row 20, column E
                            excelImage.SetPosition(1, 0, 1, 0);
                        }

                        var filtro = input.Filtro;

                        var filters = new List<object[]>
                        {
                            new object[] { 6, 2, "Supervisor", filtro.Supervisor ?? ""},
                            new object[] { 6, 5, "Clase", filtro.Clase ?? ""},
                            new object[] { 8, 2, "Departamento", filtro.Departamento ?? ""},
                            new object[] { 8, 5, "Subsidiaria", filtro.Subsidiaria ?? ""},
                        };

                        sheet.Column(1).Width = 20;

                        foreach (var cell in filters)
                        {
                            int row = (int)cell[0];
                            int col = (int)cell[1];

                            sheet.Cells[row, col].Style.Font.Bold = true;
                            sheet.Cells[row, col].Style.Font.Size = 9;

                            sheet.Cells[row, col].Value = cell[2];
                            sheet.Cells[row, col + 1].Value = cell[3].ToString().Replace("-1", "");
                        }

                        int fila = 10;

                        foreach (var sup in input.Source)
                        {
                            // Titulo de supervisor
                            sheet.Cells[fila, 2].Value = "SUPERVISOR: " + sup.Supervisor;

                            foreach (var col in new int[] { 2, 3, 4, 5, 6 })
                            {
                                sheet.Row(fila).Height = 30;

                                sheet.Cells[fila, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells[fila, col].Style.Font.Color.SetColor(whiteColor);
                                sheet.Cells[fila, col].Style.Fill.BackgroundColor.SetColor(orangeColor);


                            }

                            sheet.Cells[fila, 2, fila, 6].Merge = true;

                            // Cabecera de registros
                            fila++;

                            foreach (var col in new Dictionary<int, string> { { 2, "Nombre" }, { 3, "Cargo" }, { 4, "Clase" }, { 5, "Subsidiaria" }, { 6, "Departamento" } })
                            {
                                sheet.Row(fila).Height = 30;

                                sheet.Cells[fila, col.Key].Value = col.Value;

                                sheet.Cells[fila, col.Key].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells[fila, col.Key].Style.Font.Color.SetColor(whiteColor);
                                sheet.Cells[fila, col.Key].Style.Fill.BackgroundColor.SetColor(brownColor);

                                sheet.Cells[fila, col.Key].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                sheet.Cells[fila, col.Key].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                                sheet.Cells[fila, col.Key].Style.Border.Right.Style = ExcelBorderStyle.Thick;

                                sheet.Cells[fila, col.Key].Style.Border.Left.Color.SetColor(orangeColor);
                                sheet.Cells[fila, col.Key].Style.Border.Bottom.Color.SetColor(orangeColor);
                                sheet.Cells[fila, col.Key].Style.Border.Right.Color.SetColor(orangeColor);
                            }

                            sheet.Cells[fila, 2].Style.Border.Left.Style = ExcelBorderStyle.None;
                            sheet.Cells[fila, 6].Style.Border.Right.Style = ExcelBorderStyle.None;

                            // Datos
                            foreach (var item in sup.Items)
                            {
                                fila++;

                                foreach (var col in new Dictionary<int, string> { { 2, item.Nombre }, { 3, item.Cargo }, { 4, item.Clase }, { 5, item.Subsidiaria }, { 6, item.Departamento } })
                                {
                                    sheet.Row(fila).Height = 25;

                                    sheet.Cells[fila, col.Key].Value = col.Value;

                                    sheet.Cells[fila, col.Key].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    sheet.Cells[fila, col.Key].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    sheet.Cells[fila, col.Key].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                    sheet.Cells[fila, col.Key].Style.Border.Left.Color.SetColor(brownColor);
                                    sheet.Cells[fila, col.Key].Style.Border.Bottom.Color.SetColor(brownColor);
                                    sheet.Cells[fila, col.Key].Style.Border.Right.Color.SetColor(brownColor);
                                }
                            }

                            fila++;
                        }

                        for (int i = 1; i <= fila; i++)
                        {
                            sheet.Row(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        }

                        sheet.Cells[$"A1:F{fila}"].Style.Font.SetFromFont(new System.Drawing.Font("Courier", 12));
                        sheet.Cells[$"A1:F{fila}"].AutoFitColumns();
                        sheet.View.ShowGridLines = false;

                        memo = new MemoryStream(excel.GetAsByteArray());
                    }
                    #endregion
                }
                else
                {
                    #region PDF...

                    Document document = new Document(PageSize.A4, 20, 20, 20, 20);
                    MemoryStream memoryStream = new MemoryStream();

                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();

                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imagePath);

                    document.Add(jpg);

                    var filtro = input.Filtro;
                    var tblFiltros = new PdfPTable(4);
                    var baseFont = iTextSharp.text.Font.FontFamily.COURIER;

                    //BaseFont rockwellBold = BaseFont.CreateFont(Path.Combine(FILE_PATH, "Akrobat-Regular.ttf"), BaseFont.CP1252, BaseFont.EMBEDDED);

                    var standardFont = new iTextSharp.text.Font(baseFont, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                    var whiteFont = new iTextSharp.text.Font(baseFont, 10, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
                    var brownFont = new iTextSharp.text.Font(baseFont, 10, iTextSharp.text.Font.NORMAL, new BaseColor(brownColor));

                    tblFiltros.SpacingBefore = 20;
                    tblFiltros.WidthPercentage = 100;
                    tblFiltros.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;

                    Func<string, PdfPCell> GetCell = (label) =>
                    {
                        return new PdfPCell(new Phrase((label ?? "").Replace("-1", ""), standardFont)) { BorderWidth = 0, Padding = 5 };
                    };

                    tblFiltros.AddCell(GetCell("Supervisor"));
                    tblFiltros.AddCell(GetCell(filtro.Supervisor));
                    tblFiltros.AddCell(GetCell("Clase"));
                    tblFiltros.AddCell(GetCell(filtro.Clase));

                    tblFiltros.AddCell(GetCell("Departamento"));
                    tblFiltros.AddCell(GetCell(filtro.Departamento));
                    tblFiltros.AddCell(GetCell("Subsidiaria"));
                    tblFiltros.AddCell(GetCell(filtro.Subsidiaria));

                    document.Add(tblFiltros);



                    Func<int, string, PdfPCell> GetHeaderCell = (tipo, label) =>
                    {
                        var th = new PdfPCell(new Phrase(label ?? "", whiteFont));

                        th.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        th.Padding = 10;

                        if (tipo == 1 || tipo == 0)
                        {
                            th.BorderWidthRight = 2;
                            th.BorderColorRight = new BaseColor(orangeColor);
                            th.BackgroundColor = new BaseColor(brownColor);
                        }

                        th.BorderWidthBottom = 2;
                        th.BorderColorBottom = new BaseColor(orangeColor);
                        th.BackgroundColor = new BaseColor(brownColor);

                        if (tipo == 1 || tipo == 2)
                        {
                            th.BorderWidthLeft = 2;
                            th.BorderColorLeft = new BaseColor(orangeColor);
                            th.BackgroundColor = new BaseColor(brownColor);
                        }

                        th.BorderWidthTop = 0;

                        return th;
                    };

                    Func<bool, string, PdfPCell> GetBodyCell = (first, label) =>
                    {
                        var th = new PdfPCell(new Phrase(label ?? "", brownFont));
                        var bc = ColorTranslator.FromHtml("#D2D2D2");

                        th.Padding = 10;
                        th.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;

                        th.BorderColorTop = new BaseColor(whiteColor);
                        th.BorderColorRight = new BaseColor(bc);
                        th.BorderColorBottom = new BaseColor(bc);
                        th.BorderColorLeft = new BaseColor(bc);

                        th.BorderWidthTop = 1;
                        th.BorderWidthRight = first ? 0 : 1;
                        th.BorderWidthBottom = 1;
                        th.BorderWidthLeft = 1;

                        return th;
                    };

                    var tblData = new PdfPTable(5);

                    tblData.WidthPercentage = 100;
                    tblData.SpacingBefore = 20;

                    foreach (var sup in input.Source)
                    {
                        var th = new PdfPCell(new Phrase("SUPERVISOR: " + sup.Supervisor, whiteFont))
                        {
                            Padding = 10,
                            Colspan = 5,
                            BorderColor = new BaseColor(orangeColor),
                            BackgroundColor = new BaseColor(orangeColor)
                        };

                        tblData.AddCell(th);

                        tblData.AddCell(GetHeaderCell(0, "Nombre"));
                        tblData.AddCell(GetHeaderCell(1, "Cargo"));
                        tblData.AddCell(GetHeaderCell(1, "Clase"));
                        tblData.AddCell(GetHeaderCell(1, "Subsidiaria"));
                        tblData.AddCell(GetHeaderCell(2, "Departamento"));

                        foreach (var item in sup.Items)
                        {
                            tblData.AddCell(GetBodyCell(true, item.Nombre));
                            tblData.AddCell(GetBodyCell(false, item.Cargo));
                            tblData.AddCell(GetBodyCell(false, item.Clase));
                            tblData.AddCell(GetBodyCell(false, item.Subsidiaria));
                            tblData.AddCell(GetBodyCell(false, item.Departamento));
                        }
                    }

                    document.Add(tblData);


                    document.Close();

                    byte[] bytes = memoryStream.ToArray();

                    memo = new MemoryStream(bytes);

                    #endregion
                }
            });

            if (memo == null)
                return BadRequest();

            var formatos = new Dictionary<string, string>()
            {
                { "XLSX", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
                { "CSV", "text/csv" },
                { "PDF", "application/pdf" }
            };

            if (!formatos.ContainsKey(formato.ToUpper()))
                return BadRequest("Formato Incorrecto");

            var mimeType = formatos[formato.ToUpper()];
            var fileName = $"EmployeesReport_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.{formato.ToLower()}";

            return File(memo, mimeType, fileName);
        }
    }
}