﻿using System;
using System.Collections.Generic;

namespace KisforcePruebaTecnica.Models
{
    public class ReportInput
    {
        public ReportInput()
        {
        }

        public Employee Filtro { get; set; }
        public List<Employee> Source { get; set; }
    }
}
