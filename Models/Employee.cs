﻿using System;
using System.Collections.Generic;

namespace KisforcePruebaTecnica.Models
{
    public class Employee
    {
        public Employee()
        {
        }

        public int Fila { get; set; }
        public string Nombre { get; set; }
        public string Cargo { get; set; }
        public string Supervisor { get; set; }
        public string Clase { get; set; }
        public string Subsidiaria { get; set; }
        public string Departamento { get; set; }

        public List<Employee> Items { get; set; }
    }
}
