﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace KisforcePruebaTecnica
{
    public class ExpressionBuilder
    {
        #region New...

        public static Expression<Func<T, bool>> GetExpression<T>(IList<ExpressionFilter> filters)
        {
            if (filters.Count == 0)
                return null;

            ParameterExpression param = Expression.Parameter(typeof(T), "t");
            Expression exp = null;

            foreach (var filtro in filters)
            {
                if (exp == null)
                {
                    exp = GetExpression<T>(param, filtro);
                }
                else
                {
                    exp = Expression.AndAlso(exp, GetExpression<T>(param, filtro));
                }
            }

            //if (filters.Count == 1)
            //{
            //    exp = GetExpression<T>(param, filters[0]);
            //}
            //else if (filters.Count == 2)
            //{
            //    exp = GetExpression<T>(param, filters[0], filters[1]);
            //}
            //else
            //{
            //    while (filters.Count > 0)
            //    {
            //        var f1 = filters[0];
            //        var f2 = filters[1];

            //        if (exp == null)
            //            exp = GetExpression<T>(param, filters[0], filters[1]);
            //        else
            //            exp = Expression.AndAlso(exp, GetExpression<T>(param, filters[0], filters[1]));

            //        filters.Remove(f1);
            //        filters.Remove(f2);

            //        if (filters.Count == 1)
            //        {
            //            exp = Expression.AndAlso(exp, GetExpression<T>(param, filters[0]));
            //            filters.RemoveAt(0);
            //        }
            //    }
            //}

            return Expression.Lambda<Func<T, bool>>(exp, param);
        }

        private static BinaryExpression GetExpression<T>(ParameterExpression param, ExpressionFilter filter1, ExpressionFilter filter2)
        {
            Expression bin1 = GetExpression<T>(param, filter1);
            Expression bin2 = GetExpression<T>(param, filter2);

            return Expression.AndAlso(bin1, bin2);
        }

        private static Expression GetExpression<T>(ParameterExpression param, ExpressionFilter filter)
        {
            MemberExpression member = Expression.Property(param, filter.PropertyName);
            UnaryExpression constant = Expression.Convert(Expression.Constant(filter.Value), member.Type);

            return Expression.Equal(member, constant);
        }
        #endregion
    }
}
