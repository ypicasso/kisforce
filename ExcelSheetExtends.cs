﻿using OfficeOpenXml;
using System;

namespace KisforcePruebaTecnica
{
    public static class ExcelSheetExtends
    {
        public static string GetString(this ExcelWorksheet sheet, int row, int col)
        {
            //var valor = sheet.Cells[row, col].Value;
            return sheet.Cells[row, col].GetValue<string>()?.Trim() ?? string.Empty;
        }

        public static DateTime GetDateTime(this ExcelWorksheet sheet, int row, int col)
        {
            return sheet.Cells[row, col].GetValue<DateTime>();
        }

        public static Decimal GetDecimal(this ExcelWorksheet sheet, int row, int col)
        {
            return sheet.Cells[row, col].GetValue<decimal>();
        }
    }
}
