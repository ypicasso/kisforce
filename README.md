## Pre-requisitos

1. Tener instalado NodeJS, o lo puede descargar de [aquí](https://nodejs.org/es/).
2. Tener instalado Visual Studio 2019 Community, o lo puede descargar de [aquí](https://visualstudio.microsoft.com/es/).
3. Tener instalado .NET Core 3.1, o lo puede descargar de [aquí](https://dotnet.microsoft.com/download/dotnet-core/3.1).

---

## Ejecución de la aplicación

1. Abrir el archivo de proyecto **(.csproj)** con Visual Studio 2019 Community.
2. Abrir la línea de comandos en **(Windows)**, o la terminal en **Mac OS**.
3. Una vez mostrada la ventana solicitada, ubicarse en la carpeta **ClienteApp** del proyecto.
4. Ejecutar el siguiente comando: *npm install*.
5. Ejecute la aplicación realizando lo siguiente: 
en **Windows** de clic en el menú **Debug** y luego de clic en la opción **Start Debugging**;
en **Mac OS** de clic en el menú **Run** y luego de clic en la opción **Start Debugging**.